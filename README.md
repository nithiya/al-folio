# al-folio

al-folio Jekyll theme demo hosted on GitLab Pages: <https://nithiya.gitlab.io/al-folio/>

## Credits

A simple, clean, and responsive Jekyll theme for academics: <https://github.com/alshedivat/al-folio>

Hosted by GitLab Pages. Photos from Unsplash.

## Serving this site

1.  Install Ruby.

1.  Clone this repository:

    Option 1 - SSH:

    ```sh
    git clone git@gitlab.com:nithiya/al-folio.git
    ```

    Option 2 - HTTPS:

    ```sh
    git clone https://gitlab.com/nithiya/al-folio.git
    ```

1.  Navigate to the cloned directory and install all requirements:

    ```sh
    cd al-folio
    gem install bundler
    bundle install
    ```

1.  Serve the site:

    ```sh
    bundle exec jekyll serve
    ```

1.  View the site at `http://localhost:4000/al-folio/`.

Alternatively, docker-compose can be used to serve the site. See the al-folio GitHub repo for more details.

The GitLab CI uses Ruby 3.2.

## License

[MIT License](https://opensource.org/licenses/MIT)

This repository is maintained by Nithiya Streethran (nmstreethran at gmail dot com)
